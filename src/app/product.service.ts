import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../app/product'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private findAllProducts: string;

  private addProduct: string;

  constructor(private http: HttpClient){
    this.findAllProducts = "http://localhost:8080/products";
    this.addProduct = "http://localhost:8080/product";
  }

  public findAll(): Observable <Product[]>{
    return this.http.get<Product[]>(this.findAllProducts);
  }

  public createProduct(product: Product){
    return this.http.post<Product>(this.addProduct, product);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { ProductService } from '../product.service'
import { Product } from '../product'

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  product: Product;

  constructor(private route: ActivatedRoute, private router: Router, private productService: ProductService) {
    this.product = new Product();
   }

   onSubmit(){
     this.productService.createProduct(this.product).subscribe(result => this.gotoProductList())
   }
  gotoProductList(): void {
    this.router.navigate(['/products']);
  }

  ngOnInit(): void {
  }

}
